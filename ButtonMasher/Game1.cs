﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace ButtonMasher
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Loaded in game assets
        Texture2D buttonTexture;
        SpriteFont gameFont;
        SoundEffect clickSFX;
        SoundEffect gameEndSFX;
        Song GameMusic;

        //game state/input
        MouseState previousState;
        int score = 0;
        bool playing = false;
        bool buttonClicked = false;
        bool breakStart = false;
        float timeRemaining = 0f;
        float timeLimit = 5f;
        float breakTime = 0f;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //Load button graphic
            buttonTexture = Content.Load<Texture2D>("graphics/button");

            //load font
            gameFont = Content.Load<SpriteFont>("fonts/mainSpriteFont");

            //load SFX
            clickSFX = Content.Load<SoundEffect>("audio/buttonClick");
            gameEndSFX = Content.Load<SoundEffect>("audio/gameOver");

            GameMusic = Content.Load<Song>("audio/music");

            //start background music
            MediaPlayer.Play(GameMusic);
            MediaPlayer.IsRepeating = true;

            //see mouse
            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            //get current mouse state
            MouseState currentState = Mouse.GetState();

            Vector2 screenCentre = new Vector2
                (Window.ClientBounds.Width / 2,
                Window.ClientBounds.Height / 2);

            Rectangle buttonRect = new Rectangle
                ((int)screenCentre.X - buttonTexture.Width / 2,
                (int)screenCentre.Y - buttonTexture.Height / 2,
                buttonTexture.Width, buttonTexture.Height);

            buttonClicked = false;
            
            int counter = 0;

            //check if we have clicked
            if (currentState.LeftButton==ButtonState.Pressed && 
                previousState.LeftButton != ButtonState.Pressed && 
                buttonRect.Contains(currentState.X, currentState.Y)&&
                breakTime <= 0)
            {
                buttonClicked = true;
                clickSFX.Play();
                if (playing)
                {
                    ++score;

                }
                else
                {
                    playing = true;

                    breakStart = false;

                    timeRemaining = timeLimit;

                    score = 0;

                    counter = 0;
                }
                
            }

            if (playing == true)
            {
                //update out time remaining
                // subtract the time passed
                timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                if(timeRemaining <= 0)
                {
                    if(counter == 0)
                    {
                        breakTime = 3;
                    }
                    playing = false;
                    breakStart = true;
                    ++counter;
                    timeRemaining = 0;
                    gameEndSFX.Play();
                    
                }
            }

            if(breakStart == true)
            {
                breakTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
           
            //current state becomes previous state
            previousState = currentState;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            //find the centre of the screen
            Vector2 screenCentre = new Vector2
                (Window.ClientBounds.Width / 2,
                Window.ClientBounds.Height / 2);

            //Draw stuff
            if (buttonClicked == false)
            {
                spriteBatch.Draw(buttonTexture, new Rectangle
                    ((int)screenCentre.X - buttonTexture.Width / 2,
                    (int)screenCentre.Y - buttonTexture.Height / 2,
                    buttonTexture.Width, buttonTexture.Height), Color.LightBlue);
            }
            else
            {
                spriteBatch.Draw(buttonTexture, new Rectangle
                    ((int)screenCentre.X - buttonTexture.Width / 2,
                    (int)screenCentre.Y - buttonTexture.Height / 2,
                    buttonTexture.Width, buttonTexture.Height), Color.Blue);
            }


            // draw text

            string prompString = "Click the button to start!";
            if (playing)
                prompString = "Mash the button before the time runs out!";

            Vector2 titleSize = gameFont.MeasureString("Button Masher");
            Vector2 authorSize = gameFont.MeasureString("By Zak Mitchell");
            Vector2 prompSize = gameFont.MeasureString(prompString);

            spriteBatch.DrawString(gameFont, "Button Masher", screenCentre - new Vector2(0,100) - titleSize/2, Color.White);
            spriteBatch.DrawString(gameFont, "By Zak Mitchell", screenCentre - new Vector2(0, 80) - authorSize / 2, Color.White);

            spriteBatch.DrawString(gameFont, prompString, screenCentre - new Vector2(0, 60) - prompSize / 2, Color.White);

            spriteBatch.DrawString(gameFont, "Score: ", new Vector2(10, 10), Color.White);
            spriteBatch.DrawString(gameFont, score.ToString(), new Vector2(100, 10), Color.White);

            spriteBatch.DrawString(gameFont, "Timer: ", new Vector2(Window.ClientBounds.Width - 150, 10), Color.White);
            spriteBatch.DrawString(gameFont, timeRemaining.ToString(), new Vector2(Window.ClientBounds.Width - 50, 10), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
